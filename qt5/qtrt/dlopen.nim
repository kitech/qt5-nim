
{.passl:"-ldl".}

proc dlopen(filename: cstring, flags: int) : pointer {.importc.}
proc dlclose(handle: pointer) : cint {.importc.}
proc dlsym(handle: pointer, symbol: cstring) : pointer {.importc.}
proc dlerror() : cstring {.importc.}


const RTLD_LAZY =        0x00001
const RTLD_NOW    =     0x00002
const RTLD_BINDING_MASK =  0x3
const RTLD_NOLOAD     = 0x00004
const RTLD_DEEPBIND   = 0x00008

const RTLD_GLOBAL =     0x00100
const RTLD_LOCAL    =   0
const RTLD_NODELETE  = 0x01000

const RTLD_NEXT = cast[pointer](-1)
const RTLD_DEFAULT = nil

