

import tables
import strutils
import macros
import nimlog
import nimplus


{.push hint[XDeclaredButNotUsed]:off.}

#////////
type VRetype* = uint64 # // interface{}

type Rtvaraibles* = ref object
    inited: bool
    debugFFICall*: bool
    DebugFinalize*: bool
    qtlibs: TableRef[string,pointer] # map[string]FFILibrary{}
    ffi_call_ex_fnptr: pointer
    ffi_call_ex_fn: proc(symaddr: pointer, retype: cint, retval: pointer,
                         argc: cint, argtys: pointer, argvals: pointer)

proc newRtvaraibles() : Rtvaraibles =
    var rtvars = new Rtvaraibles
    rtvars.qtlibs = newTable[string,pointer](1)
    return rtvars

var rtvarsr = newRtvaraibles()
var rtvarsp = rtvarsr.addr

proc init_ffi_invoke(rtvars : Rtvaraibles) =
    #// lib dir prefix
    #// go arch name => android lib name
    var archs = {"386": "x86", "amd64": "x86_64", "arm": "arm", "mips": "mips"}.newTable
    var oslibexts = {"linux": "so", "darwin": "dylib", "windows": "dll"}.newTable

    var getLibDirp = proc() : string =
        discard

    # // dirp must endsWith / or ""
    var getLibFile = proc(dirp, modname : string) : string =
        if hostOS == "macosx":
            return "$#libQt5$#.$#" % [dirp, modname, oslibexts[hostOS]]
        if hostOs == "windows":
            return "$#Qt5$#.$#" % [dirp, modname, oslibexts[hostOS]]
        # // case "linux", "freebsd", "netbsd", "openbsd", "android", ...:
        return "$#libQt5$#.$#" % [dirp, modname, oslibexts["linux"]]

    var loadModule = proc(libpath: string, modname: string) : bool =
        var handle = dlopen(libpath, RTLD_NOW)
        if handle != nil:
            rtvars.qtlibs[modname] = handle
        return handle != nil

    var mods = ["Inline"]
    for modname in mods:
        let libpath = getLibFile("", modname)
        let lmok = loadModule(libpath, modname)
        if not lmok: echo "loadModule error", " ", libpath, " ", lmok
    return

proc init_so_ffi_call(rtvars: Rtvaraibles) =
    var dlhandle = rtvars.qtlibs["Inline"]
    var ex_fnptr = dlsym(dlhandle, "ffi_call_ex")
    var varex_fnptr = dlsym(dlhandle, "ffi_call_var_ex")
    rtvars.ffi_call_ex_fnptr = ex_fnptr
    rtvars.ffi_call_ex_fn = cast[ptr proc(symaddr: pointer, retype: cint, retval: pointer,
        argc: cint, argtys: pointer, argvals: pointer)](rtvars.ffi_call_ex_fnptr.addr)[]
    return

proc getrtvars*() : Rtvaraibles =
    var rtvars = cast[ptr Rtvaraibles](rtvarsp)[]
    if not rtvars.inited: system.once:
        rtvars.inited = true
        init_ffi_invoke(rtvars)
        init_so_ffi_call(rtvars)
    return rtvars


# #########
import typeinfo

# // 直接使用封装的C++ symbols。好像在这设置没有用啊，符号不同，因为参数表的处理也不同，还是要改生成的调用代码。
var UseWrapSymbols = true # // see also qtrt.UseCppSymbols TODO merge

proc refmtSymbolName(symname: string) : string =
    return if UseWrapSymbols and symname.startsWith("_Z"): "C"&symname else: symname

proc GetQtSymAddrRaw(symname: string) : pointer =
    var rtvars = getrtvars()
    for modname, handle in rtvars.qtlibs:
        var symaddr = dlsym(handle, symname)
        if symaddr == nil: continue
        return symaddr
    if rtvars.debugFFICall:
        linfo "Symbol not found: ", symname
    return nil

proc GetQtSymAddr*(symname: string) : pointer =
    var symname = refmtSymbolName(symname)
    return GetQtSymAddrRaw(symname)

proc onCtorAlloc(symname: string) =
    return

proc convArgs(args: varargs[Any]) : (seq[byte], seq[pointer]) =
    var argtys = newseq[byte](0)
    var argvals = newseq[pointer](0)
    return (argtys, argvals)

proc InvokeQtFunc6s1*(symname : string, retype : byte, args: varargs[Any]) : (VRetype, bool) =
    var rtvars = getrtvars()
    var symaddr = GetQtSymAddr(symname)
    if rtvars.debugFFICall:
        linfo "FFI Call:", symname, symaddr, retype.uint16, args.len()

    doAssert rtvars.ffi_call_ex_fnptr != nil
    var retval : uint64
    var retype2 = retype.cint
    retype2 = FFI_TYPE_POINTER.cint
    var (argtys, argvals) = convArgs(args)
    rtvars.ffi_call_ex_fn(symaddr, retype2, retval.addr, 0, argtys.dtaddr, argvals.dtaddr)
    onCtorAlloc(symname)
    return (retval, true)

macro InvokeQtFunc6*(symname : string, retype : byte, args: varargs[untyped]) : untyped =
    result = quote do:
        (0, false)

proc heheh*() =
    echo "hehehhe"
    return

{.push hint[XDeclaredButNotUsed]:off.}

