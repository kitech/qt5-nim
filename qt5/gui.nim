{.push hint[XDeclaredButNotUsed]:off.}


import qt5/unsafe
import qt5/qtrt

include "./qtgui/modtypes.nim"

{.push hint[XDeclaredButNotUsed]:off.}

include "./qtgui/modfwdecls.nim"

{.push hint[XDeclaredButNotUsed]:off.}

include "./qtgui/modfiles.nim"

{.push hint[XDeclaredButNotUsed]:off.}
