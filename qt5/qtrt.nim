
## qtrt module doc here

include "./qtrt/ffi.nim"
include "./qtrt/dlopen.nim"
include "./qtrt/ffi_invoke.nim"


type CObject* = ref object of RootObj ## All binding qt class should inherit this
    Cthis*: pointer ## Underly C object pointer
    Isqocls*: bool

proc GetCthis*(cobj: CObject) : pointer =
    ## Get underly C object pointer
    linfo "hehehe"
    cobj.Cthis

proc SetCthis*(cobj: CObject, cthis: pointer) =
    cobj.Cthis = cthis

type CObject2 = ref object of CObject

# inherit and call super overloaded method
proc GetCthis(cobj: CObject2) : pointer =
    linfo "hehehe", cobj.CObject.GetCthis
    cobj.Cthis

var cobj2 = CObject2()
linfo cobj2.GetCthis()

import macros
macro SetFinalizer*(obj: untyped , finalfn: untyped) : untyped =
    result = quote do:
        discard
macro ErrPrint*(err:bool, args: varargs[untyped]) : untyped =
    result = quote do:
        discard
macro FreeMem*(mem: untyped) : untyped =
    result = quote do:
        discard
macro defer1*(expr: untyped) : untyped =
    result = quote do:
        discard
proc CString*(s:string) : pointer =
    return nil

{.push hint[XDeclaredButNotUsed]:off.}
