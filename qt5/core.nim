{.push hint[XDeclaredButNotUsed]:off.}

import qt5/unsafe
import qt5/qtrt

# how use?
#{.experimental: "codeReordering".}

{.push hint[XDeclaredButNotUsed]:off.}

include "./qtcore/modtypes.nim"

type QVariantList = seq[QVariant]
type QObjectList = seq[QFileInfo]
type QFileInfoList = seq[QFileInfo]
type QUrlList = seq[QUrl]
type QModelIndexList = seq[QModelIndex]

# some hotfix

{.push hint[XDeclaredButNotUsed]:off.}

include "./qtcore/modfwdecls.nim"

{.push hint[XDeclaredButNotUsed]:off.}

include "./qtcore/modfiles.nim"

{.push hint[XDeclaredButNotUsed]:off.}
