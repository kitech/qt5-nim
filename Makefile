
all:
	nim c -r -p:/home/me/.nimble/mulib/ simtp.nim

dc:
	nim doc -p:/home/me/.nimble/mulib/ simtp.nim
	nim doc -p:/home/me/.nimble/mulib/ qt5/qtrt.nim
	nim doc -p:/home/me/.nimble/mulib/ qt5/core.nim
	nim doc -p:/home/me/.nimble/mulib/ qt5/gui.nim
	nim doc -p:/home/me/.nimble/mulib/ qt5/widgets.nim
	nim doc -p:/home/me/.nimble/mulib/ qt5.nim

	mv *.html docs/

wc:
	wc -l qt5/{qtcore,qtgui}/*.nim

