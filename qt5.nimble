# Package

version       = "0.1.0"
author        = "egitop"
description   = "A new awesome nimble package"
license       = "LGPL-3.0"
srcDir        = "."
skipDirs      = @["tests"]
skipFiles     = @["simtp.nim"]

#bin           = @["a"]
#installExt    = @["nim"]

# Dependencies

requires "nim >= 0.19.9"
